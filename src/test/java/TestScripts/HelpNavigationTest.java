package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.Dashboard;
import pages.DrawerPage;
import pages.FeatureShowcasePage;
import pages.HelpPage;

public class HelpNavigationTest {
    public static void HelpScreenValidation(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        //Login
        Login.submit(mpin);

        //Drawer
        Dashboard.pressButtonHamburger();

        //Help
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DrawerPage.pressHelp(driver);

        //Help Center
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        HelpPage.PressHelpCenter(driver);

        //Back
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.navigate().back();

        //Terms and Conditions
        HelpPage.PressTermsAndConditions(driver);

        //Back
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.navigate().back();

        //Privacy Notice
        HelpPage.PressPrivacyNotice(driver);

        //Back
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.navigate().back();

        //What can i do with GCash?
        HelpPage.PressWhatCanIDoWithGCash(driver);

        //SKIP
        FeatureShowcasePage.skip_feature_showcase(driver);
    }

}
