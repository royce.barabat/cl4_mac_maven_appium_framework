package TestScripts;

import com.sun.xml.internal.ws.encoding.DataHandlerDataSource;
import helper.MobileActions;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.*;

import java.util.concurrent.TimeUnit;

public class PayForGCredit {

    public static void PayForGcredit(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonHamburger();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DrawerPage.pressGScore(driver);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        MobileActions.verticalSwipeByPercentages(driver, 0.6, 0.3, .05);
        GScorePage.pressManageCreditButton(driver);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GCreditPage.pressPayButton(driver);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GCreditPage.populateAmountField(driver, "1");
        GCreditPage.pressNextButton(driver);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ConfirmPage.pressButtonConfirm(driver);
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e)  {
            e.printStackTrace();
        }
        SuccessPage.pressDoneButton(driver);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        SuccessPage.pressButtonOK(driver);
    }
}
