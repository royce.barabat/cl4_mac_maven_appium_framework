package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.*;

public class SendAngPao {
	
	public static void send(AppiumDriver<MobileElement> driver, String quantity, String amount, String message, String numberOne, String numberTwo) {
		
		//Select Send Money
		Dashboard.pressButtonSendMoney();
		
		//Select Send Ang Pao
		SendMoneyPage.pressButtonSendAngPao(driver);
		
		//Added 10 seconds wait so that the app loads completely before starting with element identification
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Enter Send Ang Pao specifics Page 1
		SendAngPaoPage.populateFieldQuantity(driver, quantity);
		SendAngPaoPage.populateFieldAmount(driver, amount);
		SendAngPaoPage.populateFieldMessage(driver, message);
		driver.hideKeyboard();
		SendAngPaoPage.populateButtonNext(driver);
		
		//Added 30 seconds wait so that the app loads completely before starting with element identification
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Enter Send Ang Pao specifics Page 2
		SendAngPaoPage.populateFieldNumberOne(driver, numberOne);
		SendAngPaoPage.populateFieldNumberTwo(driver, numberTwo);
		driver.hideKeyboard();
		SendAngPaoPage.populateButtonNext(driver);
		
		//Added 5 seconds wait so that the app loads completely before starting with element identification
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Confirm Sending
		ConfirmPage.pressButtonPay(driver);
		
		//Added 10 seconds wait so that the app loads completely before starting with element identification
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Return to Dashboard
		SuccessPage.pressButtonDoneAngPao(driver);
		SuccessPage.pressButtonOK(driver);

	}

}
