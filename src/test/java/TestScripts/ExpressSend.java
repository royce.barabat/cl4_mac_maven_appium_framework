package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.*;

public class ExpressSend  {
	
	public static void send(AppiumDriver<MobileElement> driver, String mpin, String number, String amount) throws Exception{

		// Log in
		Login.submit(mpin);
		//Select Send Money
		Dashboard.pressButtonSendMoney();
		
		//Select Express Send
		SendMoneyPage.pressButtonExpressSend(driver);
		
		//Enter Express Send specifics
		ExpressSendPage.populateFieldNumber(driver, number);
		ExpressSendPage.populateFieldAmount(driver, amount);
		driver.hideKeyboard();
		ExpressSendPage.pressButtonNext(driver);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		//Confirm Sending
		ConfirmPage.pressButtonNext(driver);
		
		//Added 10 seconds wait so that the app loads completely before starting with element identification
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Return to Dashboard
		SuccessPage.pressButtonDoneExpress(driver);
		SuccessPage.pressButtonOK(driver);
	}

}
