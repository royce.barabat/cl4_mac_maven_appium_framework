package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.*;

public class ChangeMpinTest {
    /*
    Change MPIN tests(valid and invalid test cases)
    Author: Mariz Galibut
     */
    public static void changeMpin(AppiumDriver<MobileElement> driver,String mpin, String current_mpin, String new_mpin, String verify_new_mpin) throws Exception {
        //log in
        Login.submit(mpin);

        // Navigate to Settings page
        Dashboard.pressButtonHamburger();
        DrawerPage.pressSettings(driver);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Navigate to Change MPIN screen
        SettingsPage.pressChangeMpinBtn(driver);

        // Change MPIN flow
        ChangeMpinPage.inputCurrentMpin(driver, current_mpin);
        ChangeMpinPage.inputNewMpin(driver, new_mpin);
        ChangeMpinPage.inputVerifyNewMpin(driver, verify_new_mpin);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SuccessPage.pressCloseButton(driver);
    }

    public static void invalidChangeMpin(AppiumDriver<MobileElement> driver, String mpin, String current_mpin) throws Exception{
        //log in
        Login.submit(mpin);

        // Navigate to Settings page
        Dashboard.pressButtonHamburger();
        DrawerPage.pressSettings(driver);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Navigate to Change MPIN screen
        SettingsPage.pressChangeMpinBtn(driver);
        // Change MPIN flow
        ChangeMpinPage.inputCurrentMpin(driver, current_mpin);
        ChangeMpinPage.inputNewMpin(driver, "1111");
        ChangeMpinPage.inputVerifyNewMpin(driver, "1111");
        SuccessPage.pressButtonOK(driver);
    }

    public static void emptyChangeMpin(AppiumDriver<MobileElement> driver, String mpin, String current_mpin) throws Exception {
        ///log in
        Login.submit(mpin);

        // Navigate to Settings page
        Dashboard.pressButtonHamburger();
        DrawerPage.pressSettings(driver);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Navigate to Change MPIN screen
        SettingsPage.pressChangeMpinBtn(driver);
        // Change MPIN flow
        ChangeMpinPage.inputCurrentMpin(driver, current_mpin);
        ChangeMpinPage.pressDoneButton(driver);
        SuccessPage.pressButtonOK(driver);
    }

    public static void mismatchChangeMpin(AppiumDriver<MobileElement> driver, String mpin, String current_mpin, String new_mpin, String verify_new_mpin) throws Exception{
        ///log in
        Login.submit(mpin);

        // Navigate to Settings page
        Dashboard.pressButtonHamburger();
        DrawerPage.pressSettings(driver);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Navigate to Change MPIN screen
        SettingsPage.pressChangeMpinBtn(driver);

        // Change MPIN flow
        ChangeMpinPage.inputCurrentMpin(driver, current_mpin);
        ChangeMpinPage.inputNewMpin(driver, new_mpin);
        ChangeMpinPage.inputVerifyNewMpin(driver, verify_new_mpin);
        SuccessPage.pressButtonOK(driver);
    }
}
