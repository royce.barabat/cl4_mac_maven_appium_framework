package TestScripts.eventDriven_earnMoreTickets;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.*;
import helper.MobileActions;

public class ScanToPay extends VoucherPocketPage {

    public static void earnMore9(AppiumDriver<MobileElement> driver) {

        //pressSeeMoreTask(driver);
        MobileActions.verticalSwipeByPercentages(driver, 0.9, 0.1, 0.5);
        pressScanPay(driver);
    }
}
