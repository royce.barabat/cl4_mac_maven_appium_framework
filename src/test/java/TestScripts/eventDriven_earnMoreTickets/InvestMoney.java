package TestScripts.eventDriven_earnMoreTickets;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.*;
import helper.MobileActions;

public class InvestMoney extends VoucherPocketPage {

    public static void earnMore10(AppiumDriver<MobileElement> driver) {

        //wait
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Dashboard.pressButtonClaimNow();
        //wait
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        pressEarnMoreTicks(driver);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        pressSeeMoreTask(driver);
        MobileActions.verticalSwipeByPercentages(driver, 0.9, 0.3, 0.5);
        pressInvestMoney(driver);
    }
}
