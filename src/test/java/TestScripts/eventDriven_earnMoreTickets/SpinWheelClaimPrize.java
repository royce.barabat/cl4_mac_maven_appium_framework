package TestScripts.eventDriven_earnMoreTickets;

import helper.WaitForSec;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.Dashboard;

public class SpinWheelClaimPrize extends VoucherPocketBanner {
    public static void claimPrize(AppiumDriver<MobileElement> driver) throws Exception {

        WaitForSec.waitForSeconds(5);
        Dashboard.pressButtonClaimNow();

    }
}
