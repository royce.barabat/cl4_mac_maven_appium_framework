package TestScripts.eventDriven_earnMoreTickets;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.*;
import helper.MobileActions;

public class SaveMoney extends VoucherPocketPage {

    public static void earnMore3(AppiumDriver<MobileElement> driver) {

        //wait
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Dashboard.pressButtonClaimNow();
        //wait
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        pressEarnMoreTicks(driver);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        pressSaveMoney(driver);
    }
}
