package TestScripts.eventDriven_earnMoreTickets;

import helper.WaitForSec;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.Dashboard;
import pages.*;

public class EarnMoreTicketPromoPage extends VoucherPocketPage {
    public static void earnMoreTicketPromoPage(AppiumDriver<MobileElement> driver) throws Exception {
        WaitForSec.waitForSeconds(20);
        Dashboard.pressButtonClaimNow();
        WaitForSec.waitForSeconds(20);
        pressEarnMoreTicks(driver);
    }
}
