package TestScripts.eventDriven_earnMoreTickets;

import helper.WaitForSec;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.Dashboard;
import pages.VoucherPocketPage;

public class SpinWheelTab extends VoucherPocketPage {
    public static void earnMore11(AppiumDriver<MobileElement> driver) throws Exception {


        WaitForSec.waitForSeconds(5);
        Dashboard.pressButtonClaimNow();

        WaitForSec.waitForSeconds(5);
        pressEarnMoreTicks(driver);

        WaitForSec.waitForSeconds(5);
        //pressSpinTheWheel(driver);

        WaitForSec.waitForSeconds(5);
        pressEarnMoreTicks(driver);

    }
}