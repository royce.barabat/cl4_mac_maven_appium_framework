package TestScripts;

import helper.MobileActions;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.WelcomePage;

public class WelcomeUser {

    public static void welcome(AppiumDriver<MobileElement> driver) {

        //Welcome User
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WelcomePage.pressButtonOk(driver);

        for(int i = 0; i < 5; i++) {
            WelcomePage.pressButtonAllow(driver);
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < 3; i++) {
            MobileActions.horizontalSwipeByPercentage(driver, 0.9, 0.1, 0.5);
        }

    }

}
