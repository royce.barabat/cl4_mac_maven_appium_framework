package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.*;

public class BuyRegularLoad {

    public static void buy(AppiumDriver<MobileElement> driver) {

        //Select Buy Load
        Dashboard.pressButtonBuyLoad();

        //Select Buy Load For
        BuyLoadPage.pressTabBuyLoad(driver);
        BuyLoadPage.pressButtonNext(driver);

        //Added 10 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Select Regular Amount
        //BuyLoadForPage.pressTabRegular(driver);
        BuyLoadForPage.pressButton10(driver);
        BuyLoadForPage.pressButtonNext(driver);

        //Confirm Sending
        ConfirmPage.pressButtonConfirm(driver);

    }

}
