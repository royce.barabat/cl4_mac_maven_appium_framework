package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.CashInPage;
import pages.Dashboard;
import session.BaseClass;

public class CashInGlobeStore extends BaseClass {

    public static void cashIn(AppiumDriver<MobileElement> driver) {

        //Select Cash In
        Dashboard.pressButtonCashIn();

        //Added 10 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Select Globe Store
        CashInPage.pressButtonGlobeStore(driver);

        //Added 10 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Return to Dashboard
        CashInPage.pressButtonBack(driver);
        CashInPage.pressButtonBack(driver);

    }

}
