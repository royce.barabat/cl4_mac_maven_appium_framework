package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.*;

public class RequestDeleteRequestMoney {
    /*
    This class contains the test script for the following:
    - Request money
    - Nudge Request
    - Delete Request

    Author: Mariz Galibut
    */

    public static void RequestDeleteRequest(AppiumDriver<MobileElement> driver, String mpin, String requestFrom, String amount, String note)throws Exception {

        Login.submit(mpin);
        //Select Request Money
        Dashboard.pressButtonRequestMoney();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Select New Request
        RequestMoneyPage.pressButtonNewRequest(driver);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Enter Request Send specifics
        NewRequestPage.populateFieldRequestFrom(driver, requestFrom);
        NewRequestPage.populateFieldAmount(driver, amount);
        NewRequestPage.populateFieldNote(driver, note);
        driver.hideKeyboard();
        NewRequestPage.clickButtonNext(driver);

        //Confirm Sending
        ConfirmPage.pressButtonSend(driver);


        //Added 10 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Click done button in success screen
        SuccessPage.pressButtonDoneRequest(driver);

        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Check if the requested money is displayed.
        RequestMoneyPage.checkRequestIsDisplayed(driver);

        // Open pending request
        RequestMoneyPage.pressPendingRequest(driver);
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Nudge Request
        RequestMoneyPage.nudgeRequest(driver, "Test Nudge Request");

        RequestMoneyPage.pressPendingRequest(driver);
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Delete request
        RequestMoneyPage.pressDeleteRequest(driver);
    }

}


