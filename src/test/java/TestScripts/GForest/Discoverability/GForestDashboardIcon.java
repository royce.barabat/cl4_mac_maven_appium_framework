package TestScripts.GForest.Discoverability;

import TestScripts.Login;
import helper.WaitForSec;
import org.junit.Test;
import pages.Dashboard;
import pages.GForestPage;
import session.BaseClass;


/*
Tester: Royce B. Barabat
Test Case:
 1. Validate Navigation via dashboard icon
 2. Validate navigation via Show More
 3. Validate that GForest pop-up is displayed on dashboard everytime that the user logs in to the app
 4. Validate that banner on the payment receipt page is displayed and that users will get energy points for the transaction with a link going to GCash Forest
 5.Link （Shared links）
 6.Verify navigation by GF icon in Features of the Month
 */

public class GForestDashboardIcon extends BaseClass {


    @Test()
    public void Login() throws Exception {
        Login.submit("0809");
        //Scenario 1
        Dashboard.pressGForestIcon();
        WaitForSec.waitForSeconds(10);
        GForestPage.pressGforestBackBtn();

    }

//    @After
//    public void tearDown(){
//        driver.quit();
//    }
}
