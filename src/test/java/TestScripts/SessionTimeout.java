package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import java.util.logging.Logger;
import java.util.concurrent.TimeUnit;

import pages.Dashboard;
import pages.DrawerPage;
import pages.TimeOutExceptionModal;
import session.BaseClass;

public class SessionTimeout extends BaseClass {
    /*
    This class contains the test scripts for all Android session timeout test cases.
    Author: Mariz Galibut
     */
    final Logger log = Logger.getLogger("Executing Timeout Tests");

    public static void DashBoardTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }

    public static void TransactionHistory(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressLinkTransactionHistory();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }

    public static void CashInTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonCashIn();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }

    public static void SendMoneyTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonSendMoney();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void PayBillsTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonPayBills();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void BuyLoadTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonBuyLoad();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void RequestMoneyTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonRequestMoney();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void ManageCreditTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonManageCredit();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void PayOnlineTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonPayOnline();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void PayQRTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonPayQR();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void InvestMoneyTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonInvestMoney();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }

    // Hamburger Menu Features
    public static void MyLinkedAccountsTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonHamburger();
        DrawerPage.pressMyLinkedAccounts(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void PromosTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonHamburger();
        DrawerPage.pressPromos(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void GScoreTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonHamburger();
        DrawerPage.pressGScore(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void VoucherPocketTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonHamburger();
        DrawerPage.pressVoucherPocket(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void MerchantsTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonHamburger();
        DrawerPage.pressMerchants(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void ReferFriendsTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonHamburger();
        DrawerPage.pressReferFriends(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void SettingsTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonHamburger();
        DrawerPage.pressSettings(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void HelpTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonHamburger();
        DrawerPage.pressHelp(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }

    // Show More Features
    public static void ShowMoreTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonShowMore();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }

    public static void CashOutTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonShowMore();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Dashboard.pressButtonCashOut();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void BookMoviesTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonShowMore();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Dashboard.pressButtonBookMovies();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void SaveMoneyTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonShowMore();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Dashboard.pressButtonSaveMoney();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }
    public static void BorrowLoadTimeout(AppiumDriver<MobileElement> driver, String mpin)throws Exception{
        Login.submit(mpin);
        Dashboard.pressButtonShowMore();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Dashboard.pressButtonBorrowLoad();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
        TimeOutExceptionModal.dismissSessionTimeOutModal(driver);
    }

}
