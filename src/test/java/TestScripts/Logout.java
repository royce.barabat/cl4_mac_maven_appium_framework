package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.Dashboard;
import pages.DrawerPage;

import java.io.IOException;

public class Logout {
	
	public static void out(AppiumDriver<MobileElement> driver)throws IOException {
		//Logout
		Dashboard.pressButtonHamburger();
		DrawerPage.pressLogoutButton();
		DrawerPage.pressButtonOK();

		driver.quit();
	}

}
