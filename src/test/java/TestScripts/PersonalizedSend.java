package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.Dashboard;
import pages.PersonalizedSendPage;
import pages.SendMoneyPage;

public class PersonalizedSend {
	
	public static void send(AppiumDriver<MobileElement> driver, String number) {
		
		//Select Send Money
		Dashboard.pressButtonSendMoney();
		
		//Select Personalized Send
		SendMoneyPage.pressButtonPersonalizedSend(driver);
		
		//Added 15 seconds wait so that the app loads completely before starting with element identification
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Enter Personalized Send specifics
		PersonalizedSendPage.populateFieldNumber(driver, number);
		driver.hideKeyboard();
				
		//Confirm Sending
		
		//Added 10 seconds wait so that the app loads completely before starting with element identification
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Return to Dashboard

	}

}
