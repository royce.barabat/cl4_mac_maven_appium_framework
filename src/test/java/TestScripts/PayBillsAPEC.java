package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.Dashboard;
import pages.ElectricUtilitiesPage;
import pages.PayBillsPage;

public class PayBillsAPEC {

    public static void pay(AppiumDriver<MobileElement> driver) {

        //Select Pay Bills
        Dashboard.pressButtonPayBills();

        //Added 15 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Select Electric Utilities
        PayBillsPage.pressButtonElectricUtilities(driver);

        //Added 15 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Select APEC
        ElectricUtilitiesPage.pressButtonAPEC(driver);

        /*
        ExpressSendPage.locateFieldNumber(driver).sendKeys(number);
        ExpressSendPage.locateFieldAmount(driver).sendKeys(amount);
        driver.hideKeyboard();
        ExpressSendPage.locateButtonNext(driver).click();
        ExpressSendPage.locateButtonNext(driver).click();

        //Confirm Sending
        ConfirmPage.locateButtonNext(driver).click();

        //Added 10 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Return to Dashboard
        SuccessPage.locateButtonDone(driver).click();
        SuccessPage.locateButtonOK(driver).click();
        */
    }

}
