package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.*;

public class SpinWheelFromPopUp {

    public static void spin(AppiumDriver<MobileElement> driver) {

        Dashboard.pressButtonClaimNow();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        VoucherPocketPage.pressButtonSpinTheWheel(driver);
    }
}
