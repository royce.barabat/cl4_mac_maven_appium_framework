package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.Dashboard;
import pages.DrawerPage;
import pages.VoucherPocketPage;

public class VoucherPocketNavigation {
    public static void navigateToVoucherPocketTabs(AppiumDriver<MobileElement> driver){
        // Navigate to Voucher Pocket page
        Dashboard.pressButtonHamburger();
        DrawerPage.pressVoucherPocket(driver);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Navigate to Used tab
        VoucherPocketPage.pressUsedTab(driver);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // Navigate to Expired Tab
        VoucherPocketPage.pressExpiredTab(driver);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Navigate to Available Tab
        VoucherPocketPage.pressExpiredTab(driver);
    }

}
