package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.CashInPage;
import pages.Dashboard;
import session.BaseClass;

public class CashInBayadCenter extends BaseClass {

    public static void cashIn(AppiumDriver<MobileElement> driver) {

        //Select Cash In
        Dashboard.pressButtonCashIn();

        //Added 5 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Select Bayad Center
        CashInPage.pressButtonBayadCenter(driver);

        //Added 5 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Return to Dashboard
        CashInPage.pressButtonBack(driver);
        CashInPage.pressButtonBack(driver);

    }

}
