package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.AdendimunPage;
import pages.Dashboard;
import pages.ElectricUtilitiesPage;
import pages.PayBillsPage;

public class PayBillsAdendimun {

    public static void pay(AppiumDriver<MobileElement> driver, String amount, String refNum, String email) {

        //Select Pay Bills
        Dashboard.pressButtonPayBills();

        //Added 15 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Select Electric Utilities
        PayBillsPage.pressButtonElectricUtilities(driver);

        //Added 15 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Select Adendimun
        ElectricUtilitiesPage.pressButtonAdendimun(driver);

        //Added 15 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Enter Pay Adendum specifics
        AdendimunPage.populateFieldAmount(driver, amount);
        AdendimunPage.populateFieldAccountRefNum(driver, refNum);
        AdendimunPage.populateFieldEmail(driver, email);
        driver.hideKeyboard();
        AdendimunPage.pressButtonNext(driver);
        /*
        //Confirm Sending
        ConfirmPage.locateButtonNext(driver).click();

        //Added 10 seconds wait so that the app loads completely before starting with element identification
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Return to Dashboard
        SuccessPage.locateButtonDone(driver).click();
        SuccessPage.locateButtonOK(driver).click();
        */
    }

}
