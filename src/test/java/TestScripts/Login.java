package TestScripts;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import helper.WaitForSec;
import pages.LoginPage;
import session.BaseClass;

public class Login extends BaseClass {

    public static void submit(String MPIN) throws Exception {


        System.out.println("ENTERING MPIN. . . .");
        Thread.sleep(2000);
		/*
		Parameterize MPIN Test
		Author: Mariz Galibut
		 */
        if (MPIN.length() != 4) {
            return;
        }
        for (char a : MPIN.toCharArray()) {
            if (a == '0')
                LoginPage.pressButton0(driver);
            else if (a == '1')
                LoginPage.pressButton1(driver);
            else if (a == '2')
                LoginPage.pressButton2(driver);
            else if (a == '3')
                LoginPage.pressButton3(driver);
            else if (a == '4')
                LoginPage.pressButton4(driver);
            else if (a == '5')
                LoginPage.pressButton5(driver);
            else if (a == '6')
                LoginPage.pressButton6(driver);
            else if (a == '7')
                LoginPage.pressButton7(driver);
            else if (a == '8')
                LoginPage.pressButton8(driver);
            else
                LoginPage.pressButton9(driver);
        }

        System.out.println(". . . .Waiting for dashboard to load");
        WaitForSec.waitForSeconds(10);
    }
}