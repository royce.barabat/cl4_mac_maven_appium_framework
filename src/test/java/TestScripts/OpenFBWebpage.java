package TestScripts;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pages.FollowUsPage;
import pages.LoginPage;
import pages.WelcomePage;

public class OpenFBWebpage {

    public static void open(AppiumDriver<MobileElement> driver) {

        //Go to Login then Click Follow Us
        WelcomePage.pressButtonLogin(driver);
        LoginPage.pressLinkFollowUs(driver);

        //Click FB
        FollowUsPage.pressLinkFacebook(driver);

    }

}
