package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class PayBillsPage {

    //Locate then press the Pay Bills elements
    public static void pressButtonElectricUtilities(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Electric Utilities']")).click();
    }
    public static void pressButtonWaterUtilities(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Water Utilities']")).click();
    }
    public static void pressButtonCableInternet(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Cable/Internet']")).click();
    }
    public static void pressButtonTelecoms(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Telecoms']")).click();
    }

}
