package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class ChangeMpinPage {
    /*
    Change MPIN page methods
    Author: Mariz Galibut
     */
    public static void inputCurrentMpin(AppiumDriver<MobileElement> driver, String current_mpin){
        driver.findElement(By.id("txt_current_pin")).sendKeys(current_mpin);
    }
    public static void inputNewMpin(AppiumDriver<MobileElement> driver, String new_mpin){
        driver.findElement(By.id("et_new_mpin")).sendKeys(new_mpin);
    }
    public static void inputVerifyNewMpin(AppiumDriver<MobileElement> driver, String verify_new_mpin){
        driver.findElement(By.id("et_verify_new_mpin")).sendKeys(verify_new_mpin);
    }
    public static void pressDoneButton(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("btn_submit")).click();
    }

}
