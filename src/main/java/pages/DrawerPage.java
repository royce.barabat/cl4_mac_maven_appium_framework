package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import session.BaseClass;

import java.util.logging.Logger;

public class DrawerPage extends BaseClass {
    private static final Logger log = Logger.getLogger("Executing Timeout Tests");

    public static void pressMyLinkedAccounts(AppiumDriver<MobileElement> driver){
        log.info("Navigate to My Linkked Accounts");
        driver.findElement(By.id("my_account_wrapper")).click();
    }

    public static void pressGScore(AppiumDriver<MobileElement> driver){
        log.info("Navigate to GScore screen");
        driver.findElement(By.id("gscore_wrapper")).click();
    }

    public static void pressPromos(AppiumDriver<MobileElement> driver){
        log.info("Navigate to Promos screen");
        driver.findElement(By.id("promos_wrapper")).click();
    }

    public static void pressVoucherPocket(AppiumDriver<MobileElement> driver){
         log.info("Navigate to Voucher Pocket screen");
        driver.findElement(By.id("voucher_wrapper")).click();
    }

    public static void pressMerchants(AppiumDriver<MobileElement> driver){
        log.info("Navigate to Merchants screen");
        driver.findElement(By.id("merchants_wrapper")).click();
    }

    public static void pressReferFriends(AppiumDriver<MobileElement> driver){
        log.info("Navigate to refer Refer Friends screen");
        driver.findElement(By.id("invite_friends_wrapper")).click();
    }

    public static void pressSettings(AppiumDriver<MobileElement> driver){
        log.info("Navigate to Settings screen");
        driver.findElement(By.id("settings_wrapper")).click();
    }

    public static void pressHelp(AppiumDriver<MobileElement> driver){
        log.info("Navigate to Help screen");
        driver.findElement(By.id("help_wrapper")).click();
    }

    public static void pressViewBenefits(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("logout_wrapper")).click();
    }

    public static void pressLogoutButton(){
        log.info("Tap Log out button");
        driver.findElement(By.id("logout_wrapper")).click();
    }
    public static void pressLinkLogout(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("logout_wrapper")).click();
    }
    public static void pressButtonOK() {
        driver.findElement(By.id("button1")).click();
    }

}
