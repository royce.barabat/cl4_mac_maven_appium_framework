package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class OtpPage {
    public static void submitOtp(AppiumDriver<MobileElement> driver) {
        //This method will submit the received OTP
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("btn_submit")).click();
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//android.widget.TextView[@text='Enter your MPIN']")).isDisplayed();

    }
}
