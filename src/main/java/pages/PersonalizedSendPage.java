package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class PersonalizedSendPage {
	
	//Locate then populate/press the Personalized Send elements
	public static void populateFieldNumber(AppiumDriver<MobileElement> driver, String number) {
		driver.findElement(By.xpath("//android.widget.EditText[@index=0]")).sendKeys(number);
	}
	
}
