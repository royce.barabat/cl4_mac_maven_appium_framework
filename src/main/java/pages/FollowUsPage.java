package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class FollowUsPage {

    //Locate then press/populate the Follow Us elements
    public static void pressButtonBack(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']")).click();
    }
    public static void pressLinkFacebook(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("img_facebook")).click();
    }
    public static void pressLinkInstagram(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("img_ig")).click();
    }
    public static void pressLinkTwitter(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("img_twitter")).click();
    }

}
