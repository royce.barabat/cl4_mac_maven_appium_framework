package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

import java.util.concurrent.TimeUnit;

public class SendMoneyPage {

	//Locate then press the Send Money elements
	public static void pressButtonExpressSend(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.xpath("//android.widget.TextView[@text='Express Send']")).click();
	}
	public static void pressButtonPersonalizedSend(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.xpath("//android.widget.TextView[@text='Personalized Send']")).click();
	}
	public static void pressButtonSendAngPao(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.xpath("//android.widget.TextView[@text='Send Ang Pao']")).click();
	}
	public static void pressButtonSendToBank(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.xpath("//android.widget.TextView[@text='Send to Bank']")).click();
	}

}
