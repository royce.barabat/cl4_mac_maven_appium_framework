package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class FeatureShowcasePage {

    public static void skip_feature_showcase(AppiumDriver<MobileElement> driver) {
        MobileElement skip_btn = driver.findElement(By.xpath("//android.widget.TextView[@text='SKIP']"));
        Boolean skip_btn_is_displayed = skip_btn.isDisplayed();

        if (skip_btn_is_displayed) {
            skip_btn.click();
        }
    }

}