package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class PopUpPage {

    public static void skipPopUpPage(AppiumDriver<MobileElement> driver) {
        MobileElement remind_me_later_btn = driver.findElement(By.xpath("//android.widget.TextView[@text='Remind me later']"));
        Boolean remind_me_later_btn_is_displayed = remind_me_later_btn.isDisplayed();

        if (remind_me_later_btn_is_displayed) {
            remind_me_later_btn.click();
        }
    }
}