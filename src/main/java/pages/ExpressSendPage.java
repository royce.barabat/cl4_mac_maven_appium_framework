package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class ExpressSendPage {

	//Locate then populate/press the Express Send elements
	public static void populateFieldNumber(AppiumDriver<MobileElement> driver, String number) {
		driver.findElement(By.id("txt_number")).sendKeys(number);
	}
	public static void populateFieldAmount(AppiumDriver<MobileElement> driver, String amount) {
		driver.findElement(By.id("txt_amount")).sendKeys(amount);
	}
	public static void pressButtonNext(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_next")).click();
	}
	public static void pressButtonConfirm(AppiumDriver<MobileElement> driver){
		driver.findElement(By.id("btn_confirm")).click();
	}
	
}
