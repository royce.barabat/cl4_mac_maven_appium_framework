package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class TimeOutExceptionModal {
    /*
    This class contains the test script for dismissing the Timeout exception modal
    Author: Mariz Galibut
     */

    //Locate and click timeout modal
    public static void dismissSessionTimeOutModal(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("button1")).click();
    }
}
