package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class SendToBankPage {

	//Express Send elements
	private static MobileElement fieldNumber = null;
	private static MobileElement fieldAmount = null;
	private static MobileElement buttonNext = null;
	
	//Locate the elements
	public static MobileElement locateFieldNumber(AppiumDriver<MobileElement> driver) {
		fieldNumber = driver.findElement(By.id("txt_number"));
		return fieldNumber;
	}
	public static MobileElement locateFieldAmount(AppiumDriver<MobileElement> driver) {
		fieldAmount = driver.findElement(By.id("txt_amount"));
		return fieldAmount;
	}
	public static MobileElement locateButtonNext(AppiumDriver<MobileElement> driver) {
		buttonNext = driver.findElement(By.id("btn_next"));
		return buttonNext;
	}
	
}
