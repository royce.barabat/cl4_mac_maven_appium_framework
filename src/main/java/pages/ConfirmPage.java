package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class ConfirmPage {
	
	//Locate then press the Confirm elements
	public static void pressButtonConfirm(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_confirm")).click();
	}
	public static void pressButtonNext(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_next")).click();
	}
	public static void pressButtonPay(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.xpath("//android.widget.Button[@text='Pay']")).click();
	}
	public static void pressButtonSend(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("viewConfirmationAction")).click();
	}
	public static void pressButtonOk(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("button1")).click();
	}


}
