package pages;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import session.BaseClass;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;


public class Dashboard extends BaseClass {
	private static final Logger log = Logger.getLogger("PayBillsGlobePostPaidGCash");

	//Locate then press the Dashboard button elements
	public static void pressButtonCashIn() {
		log.info("Navigate to Cash-In screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Cash-In']")).click();
	}
	public static void pressButtonSendMoney() {
		log.info("Navigate to Send Money screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Send Money']")).click();
	}
	public static void pressButtonPayBills() {
		log.info("Navigate to Pay Bills screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Pay Bills']")).click();
	}
	public static void pressButtonBuyLoad() {
		log.info("Navigate to Buy Load screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Buy Load']")).click();
	}
	public static void pressButtonRequestMoney() {
		log.info("Navigate to Request Money screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Request Money']")).click();
	}
	public static void pressButtonManageCredit() {
		log.info("Navigate to Manage Credit screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Manage Credit']")).click();
	}
	public static void pressButtonPayOnline() {
		log.info("Navigate to Pay Online screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Pay Online']")).click();
	}
	public static void pressButtonPayQR() {
		log.info("Navigate to Pay QR screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Pay QR']")).click();
	}
	public static void pressButtonInvestMoney() {
		log.info("Navigate to Invest Money screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Invest Money']")).click();
	}
	public static void pressButtonBankTransfer() {
		log.info("Navigate to Bank Transfer  screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Bank Transfer']")).click();
	}
	public static void pressButtonSnapMoney() {
		log.info("Navigate to Snap Money screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Personalized Send']")).click();
	}
	public static void pressButtonShowMore() {
		log.info("Tap Show More");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Show More']")).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}
	public static void pressLinkTransactionHistory() {
		log.info("Navigate to Transaaction History screen");
		driver.findElement(By.id("recent_transaction_view")).click();
	}
	public static void pressLinkInbox() {
		log.info("Navigate to Inbox screen");
		driver.findElement(By.id("notification_banner_layout")).click();
	}
	public static void pressButtonHamburger(){
		log.info("Open hamburger menu");
		driver.findElement(By.id("toolbar_icon")).click();
	}

	// Show More Features
	public static void pressButtonCashOut(){
		log.info("Navigate to Cash Out screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Cash-Out']")).click();
	}
	public static void pressButtonBookMovies(){
		log.info("Navigate to Book Movies screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Book Movies']")).click();
	}
	public static void pressButtonSaveMoney(){
		log.info("Navigate to Save Money screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Save Money']")).click();
	}
	public static void pressButtonBorrowLoad(){
		log.info("Navigate to Borrow Load screen");
		driver.findElement(By.xpath("//android.widget.TextView[@text='Borrow Load']")).click();
	}

	//Locate then press Voucher Pop-up elements
	public static void pressButtonClaimNow() {
		log.info("Press Claim Now");
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView")).click();
	}
	public static void pressButtonRemindMeLater() {
		log.info("Press Remind Me Later");
		driver.findElement(By.id("btn_remind_me_later")).click();
	}

	//GForest
	public static void pressGForestIcon() {
		driver.findElement(By.xpath("//android.widget.TextView[@text='GForest']")).click();
		System.out.println("Press GForest Icon successful. . .");
	}
}
