package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class BuyLoadPage {

    //Locate then press the Buy Load elements
    public static void pressTabBuyLoad(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='BUY LOAD']")).click();
    }
    public static void pressTabBorrowLoad(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='BORROW LOAD']")).click();
    }
    public static void pressTabFavorites(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='FAVORITES']")).click();
    }
    public static void pressButtonNext(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='NEXT']")).click();
    }

}
