package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

import java.util.NoSuchElementException;

public class VoucherPocketPage {

    public static void pressUsedTab(AppiumDriver<MobileElement> driver) {
        // Tap and check if Used Tab is enabled
        MobileElement used_tab = driver.findElement(By.xpath("//android.view.View[@text='Used']"));
        used_tab.click();
        Boolean isSelected = used_tab.isEnabled();
        try {
            if (isSelected) {
                System.out.println("Element is enabled");
            }
        } catch (NoSuchElementException e) {
            throw new RuntimeException("Element is not enabled");
        }
    }

    public static void pressExpiredTab(AppiumDriver<MobileElement> driver){
        // Tap and check is Expired Tab is enabled
        MobileElement expired_tab = driver.findElement(By.xpath("//android.view.View[@text='Expired']"));
        expired_tab.click();
        Boolean isSelected = expired_tab.isEnabled();
        try {
            if (isSelected){
                System.out.println("Element is enabled");
            }
        }
        catch (NoSuchElementException e) {
            throw new RuntimeException("Element is not enabled");
        }


    }

    public static void pressAvailableTab(AppiumDriver<MobileElement> driver){
        // Tap and check if Available Tab is enabled
        MobileElement available_tab = driver.findElement(By.xpath("//android.view.View[@text='Available']"));
        available_tab.click();
        Boolean isSelected = available_tab.isEnabled();
        try {
            if (isSelected){
                System.out.println("Element is enabled");
            }
        }
        catch (NoSuchElementException e) {
            throw new RuntimeException("Element is not enabled");
        }
    }
    //Spin to win promo press "back" button
    public static void pressBack(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc='Back']")).click();
    }
    //Spin to win promo press "see more tasks"
    public static void pressSeeMoreTask(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='SEE MORE TASKS ∨']")).click();
    }
    //Raffle press "Spin the wheel"
    public static void pressButtonSpinTheWheel(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='SPIN THE WHEEL']")).click();
    }
    //Raffle press "Earn more tickets"
    public static void pressEarnMoreTicks(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='Earn more tickets >']")).click();
    }
    //Spin to win promo "cash-in"
    public static void pressCashIn(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.Image[@text='a1082a11-182d-4363-abba-7b6dc1179f86_w684_h178']")).click();
    }
    //Spin to win promo "send money"
    public static void pressSendMoney(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.Image[@text='5a95adf5-d903-4bb4-adce-06638e817713_w684_h178']")).click();
    }
    //Spin to win promo "save money"
    public static void pressSaveMoney(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.Image[@text='c7a3fb21-bcf1-4b31-9b7a-d6e7b90cf4c1_w684_h178']")).click();
    }
    //Spin to win promo "Shop at lazada"
    public static void pressShopAtLazada(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.Image[@text='07289153-cca8-4bd4-8ccb-88dd197f4d4d_w684_h178']")).click();
    }
    //Spin to win promo "refer friends"
    public static void pressReferFriends(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.Image[@text='54a70a88-1ca6-46e7-8e3b-5799ef2d61e6_w684_h178']")).click();
    }
    //Spin to win promo "Buy Gaming Pins"
    public static void pressBuyGPins(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.Image[@text='2985837d-fb49-4571-a1be-5a51892300b3_w684_h178']")).click();
    }
    //Spin to win promo "Buy Broadband"
    public static void pressBuyBroadband(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.Image[@text='72adc586-53a3-4a0c-9339-61277ebcda1e_w342_h89']")).click();
    }
    //Spin to win promo "Pay utility bills"
    public static void pressUtilBills(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.Image[@text='fa06f4a2-c860-48bb-a764-5b3b3685651d_w684_h178']")).click();
    }
    //Spin to win promo "Scan to Pay"
    public static void pressScanPay(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.Image[@text='6abff5b6-877e-4548-9fc7-9543ba0deabe_w684_h178']")).click();
    }
    //Spin to win promo "Invest Money"
    public static void pressInvestMoney(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.Image[@text='ff0b78ef-6caf-4daa-b08e-1e468b0a58ff_w684_h178']")).click();
    }
}
