package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class CashInPage {

    //Locate then press the Cash In elements
    public static void pressButtonPuregold(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='Puregold']")).click();
    }
    public static void pressButtonBayadCenter(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='Bayad Center']")).click();
    }
    public static void pressButtonECPay(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='ECPay']")).click();
    }
    public static void pressButtonExpresspay(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='Expresspay']")).click();
    }
    public static void pressButtonDigipay(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='Digipay']")).click();
    }
    public static void pressButtonGlobeStore(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='Globe Store']")).click();
    }
    public static void pressButtonSM(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='SM']")).click();
    }
    public static void pressButtonRobinsons(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='Robinsons']")).click();
    }
    public static void pressButtonCebuanaLhuiller(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='Cebuana Lhuiller']")).click();
    }
    public static void pressButtonVillarica(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='Villarica']")).click();
    }
    public static void pressButtonRD(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='RD']")).click();
    }
    public static void pressButtonTambunting(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='Tambunting']")).click();
    }
    public static void pressButton7Eleven(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='7-Eleven']")).click();
    }
    public static void pressButtonShellSelect(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='Shell Select']")).click();
    }
    public static void pressButtonTouchPay(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='TouchPay']")).click();
    }
    public static void pressButtoneTap(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.view.View[@text='eTap / Pay & Go']")).click();
    }
    public static void pressButtonBack(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("h5_tv_nav_back")).click();
    }

}
