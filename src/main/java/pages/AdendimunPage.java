package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class AdendimunPage {

    //Locate then populate/press the Adendimun elements
    public static void populateFieldAmount(AppiumDriver<MobileElement> driver, String amount) {
        driver.findElement(By.xpath("//android.widget.EditText[@text='Enter Amount in Php (1-8 digits)']")).sendKeys(amount);
    }
    public static void populateFieldAccountRefNum(AppiumDriver<MobileElement> driver, String refNum) {
        driver.findElement(By.xpath("//android.widget.EditText[@text='Enter Account Ref No. (8 digits)']")).sendKeys(refNum);
    }
    public static void populateFieldEmail(AppiumDriver<MobileElement> driver, String email) {
        driver.findElement(By.xpath("//android.widget.EditText[@text='Enter Email Address (optional)']")).sendKeys(email);
    }
    public static void pressButtonNext(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("btn_next")).click();
    }

}
