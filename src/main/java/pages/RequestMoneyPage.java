package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class RequestMoneyPage {
    /*
    This class contains all the actions inside the request money component.
    Author: Mariz Galibut
     */

    //Locate then press the Send Money elements
    public static void pressLinkRequestsSent(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='REQUESTS SENT']")).click();
    }
    public static void pressLinkRequestsReceived(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='REQUESTS RECEIVED']")).click();
    }
    public static void pressButtonNewRequest(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='NEW REQUEST']")).click();
    }
    public static void pressViewHistory(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("viewHistory")).click();
    }
    public static void pressAbout(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("action_info")).click();
    }
    public static void pressPendingRequest(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("itemContainer")).click();
    }
    public static void pressDeleteRequest(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("ivTransactionDecline")).click();
        driver.findElement(By.id("button1")).click();
        driver.findElement(By.id("button1")).click();
    }
    public static void nudgeRequest(AppiumDriver<MobileElement> driver, String message){
        driver.findElement(By.id("ivTransactionAccept")).click();
        driver.findElement(By.id("etNudgeMessage")).sendKeys(message);
        driver.hideKeyboard();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("viewNudgeOk")).click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ConfirmPage.pressButtonOk(driver);

    }
    public static void checkRequestIsDisplayed(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("itemContainer")).isDisplayed();
        driver.findElement(By.id("textview_tab_badge")).isDisplayed();
    }

}
