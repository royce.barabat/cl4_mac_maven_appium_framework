package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class SettingsPage {
    /*
    Setting page elements and methods
    Author: Mariz Galibut
     */

    public static void pressChangeMpinBtn(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("btn_change_pin")).click();
    }
    public static void pressAccountauthenticationBtn(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("btn_account_recovery")).click();
    }
    public static void pressSetReminderBtn(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("wrapperSetReminder")).click();
    }


}
