package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class PayOnlinePage {

    //Locate then press the Pay Online elements
    public static void clickLinkPayWithGCash(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("txt_pay_gcash")).click();
    }
    public static void clickLinkPayWithAmex(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("txt_pay_amex")).click();
    }
    public static void clickLinkPayWithMastercard(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("txt_pay_mastercard")).click();
    }
    public static void clickLinkSeeAll(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("txt_see_all")).click();
    }

}
