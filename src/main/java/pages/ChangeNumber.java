package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class ChangeNumber {

    public static  void changeNumber(AppiumDriver<MobileElement> driver, String number){

        // Wait for the element to appear
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.id("btn_change_number")).click();

        // Wait for the element to appear
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.id("button1")).click();
        driver.findElement(By.id("txt_msisdn")).sendKeys(number);
        driver.findElement(By.id("btn_next")).click();

        // Wait for the element to appear
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("btn_submit")).isDisplayed();
    }
}
