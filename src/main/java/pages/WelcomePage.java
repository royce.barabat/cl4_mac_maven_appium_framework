package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class WelcomePage{

    //Locate then press the Permission modal elements
    public static void pressButtonOk(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("button1")).click();
    }
    public static void pressButtonAllow(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.Button[@text='Allow']")).click();
    }

    //Locate then press the Welcome page elements
    public static void pressButtonLogin(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("btn_welcome_login")).click();
    }
    public static void pressButtonRegister(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("btn_welcome_register")).click();
    }

}
