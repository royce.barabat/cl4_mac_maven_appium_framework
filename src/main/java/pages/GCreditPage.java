package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class GCreditPage {
    /*
    This class contains the components and methods inside the GCredit page
    Author: Mariz Galibut
     */
    public static void pressInfoButton(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("action_info")).click();
    }
    public static void pressPayButton(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("btn_payment")).click();
    }
    public static void pressTransactionHistory(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("recent_transaction_view")).click();
    }
    public static void pressMyGScoreLink(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("wrapper_gscore")).click();
    }


    // Pay for GCredit Screen
    public static void populateAmountField(AppiumDriver<MobileElement> driver, String amount){
        driver.findElement(By.id("txt_amount")).sendKeys(amount);
    }
    public static void pressNextButton(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("btn_next")).click();
    }


}

