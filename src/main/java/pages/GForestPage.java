package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import session.BaseClass;


public class GForestPage extends BaseClass {
    //Back button
    public static void pressGforestBackBtn() {
        driver.findElement(By.id("h5_tv_nav_back")).click();
        System.out.println("Press GForest back button successfully!");
    }
    //GForest Initial Screen
    //I want to help button
    public static void pressGforestIwanToHelpBtn() {
        driver.findElement(By.xpath("GForest help btn xpath/id")).click();
        System.out.println("Press GForest I want to help button successfully!");
    }
    //Terms of agreement
    public static void pressGforestTOA() {
        driver.findElement(By.xpath("GForest TOA xpath/id")).click();
        System.out.println("Press GForest terms of agreement successfully!");
    }
    //Allow access button
    public static void pressGforestAllowAccessBtn() {
        driver.findElement(By.xpath("//android.view.View[@text='ALLOW ACCESS']")).click();
        System.out.println("Press GForest allow access button successfully!");
    }
    //Maybe later button
    public static void pressGforestMaybeLtrBtn() {
        driver.findElement(By.xpath("//android.view.View[@text='MAYBE LATER']")).click();
        System.out.println("Press GForest maybe later button successfully!");
    }

    //Discoverability GF1.1
    //GForest Pop-up displayed
    public static void pressGforestPopup() {
        driver.findElement(By.id("card_promo")).click();
        System.out.println("Press GForest pop-up successfully!");
    }
    //Banner on the payment receipt page
    public static void pressGforestBanner() {
        driver.findElement(By.xpath("GForest banner xpath/id")).click();
        System.out.println("Press GForest banner successfully!");
    }

    //Account Setup
    //Consent Page GF2.1
    public static void pressGforestConsentBtn() {
        driver.findElement(By.xpath("GForest consent btn object xpath/id")).click();
        System.out.println("Press GForest consent button successfully!");
    }
    //Display initiative details
    public static void initiativeDetails() {
        driver.findElement(By.xpath("GForest initiative details object xpath/id")).click();
        System.out.println("Press GForest <object> successfully!");
    }
    //Display button asking for user's consent to join GCash
    public static void pressGforestJoinBtn() {
        driver.findElement(By.xpath("GForest join GForest btn object xpath/id")).click();
        System.out.println("Press GForest <object> successfully!");
    }


    //T and C page GF2.2
    public static void pressGforestTCLink() {
        driver.findElement(By.xpath("GForest T and C link xpath/id")).click();
        System.out.println("Press GForest T & C link successfully!");
    }

    //Third PT App GF2.3
    //Phone Contacts
    public static void phoneContactsPage() {
        driver.findElement(By.xpath("GForest phone contacts xpath/id")).click();
        System.out.println("Press GForest <object> successfully!");
    }
    //Facebook
    public static void facebookPage() {
        driver.findElement(By.xpath("GForest user solve problem page object xpath/id")).click();
        System.out.println("Press GForest <object> successfully!");
    }
    //Health App
    public static void healthAppPage() {
        driver.findElement(By.xpath("GForest user solve problem page object xpath/id")).click();
        System.out.println("Press GForest <object> successfully!");
    }

    //Forest Dashboard
    //Tree Growth GF3.1
    //5 stages of tree growth
    public static void treeGrowthPage() {
        driver.findElement(By.xpath("GForest tree growth page object xpath/id")).click();
        System.out.println("Press GForest <object> successfully!");
    }
    //Energy bubbles GF3.2
    public static void pressEnergyBubbles() {
        driver.findElement(By.xpath("GForest energy bubbles object xpath/id")).click();
        System.out.println("Press GForest energy bubbles successfully!");
    }
    //Total energy points GF3.3
    public static void totalEnergyPoints() {
        driver.findElement(By.xpath("GForest total energy points object xpath/id")).click();
        System.out.println("Press GForest <object> successfully!");
    }

}
