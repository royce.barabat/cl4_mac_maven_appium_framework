package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import org.openqa.selenium.By;

import java.io.IOException;

public class LoginPage {

	//Locate then press/populate the Login page fields
	public static void populateFieldMobTel(AppiumDriver<MobileElement> driver, String mobTel) {
		driver.findElement(By.id("txt_msisdn")).sendKeys(mobTel);
	}
	public static void pressButtonNext(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_next")).click();
	}
	public static void pressButtonSubmit(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_submit")).click();
	}
	public static void pressLinkHelpCenter(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("tvFaqs")).click();
	}
	public static void pressLinkFollowUs(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("tvFollowus")).click();
	}


	//Locate then press the MPIN button elements
	public static void pressButton0(AppiumDriver<MobileElement> driver) throws IOException {
		driver.findElement(By.id("btn_number0")).click();
	}
	public static void pressButton1(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_number1")).click();
	}
	public static void pressButton2(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_number2")).click();
	}
	public static void pressButton3(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_number3")).click();
	}
	public static void pressButton4(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_number4")).click();
	}
	public static void pressButton5(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_number5")).click();
	}
	public static void pressButton6(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_number6")).click();
	}
	public static void pressButton7(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_number7")).click();
	}
	public static void pressButton8(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_number8")).click();
	}
	public static void pressButton9(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_number9")).click();
	}
	public static void pressTryAgain(AppiumDriver<MobileElement> driver){
		driver.findElement(By.xpath("//android.widget.Button[@text='Try Again!']")).click();

	}

}
