package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class SuccessPage {
	
	//Locate then press the Success elements
	public static void pressButtonDoneExpress(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("btn_done")).click();
	}
	public static void pressButtonOK(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("button1")).click();
	}
	public static void pressButtonDoneAngPao(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.id("h5_bt_text")).click();
	}
	public static void pressButtonDoneRequest(AppiumDriver<MobileElement> driver){
		driver.findElement(By.xpath("//android.widget.TextView[@text='Done']")).click();
	}
	public static void pressDoneButton(AppiumDriver<MobileElement> driver){
		driver.findElement(By.id("btn_done")).click();
	}
	public static void pressCloseButton(AppiumDriver<MobileElement> driver){
		driver.findElement(By.id("x")).click();
	}
	
}
