package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class ElectricUtilitiesPage {

    //Locate then press the Electric Utilities elements
    public static void pressButtonAdendimun(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Adendimun']")).click();
    }
    public static void pressButtonAPEC(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='APEC']")).click();
    }
    public static void pressButtonAngelesElectric(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Angeles Electric']")).click();
    }
    public static void pressButtonANTECO(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='ANTECO']")).click();
    }

}
