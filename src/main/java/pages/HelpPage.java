package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import java.util.logging.Logger;


public class HelpPage {
    private static final Logger log = Logger.getLogger("Help");

    public static void PressHelpCenter(AppiumDriver<MobileElement> driver){
        log.info("Navigate to Help Center screen");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Help Center']")).click();
    }
    public static void PressTermsAndConditions(AppiumDriver<MobileElement> driver) {
        log.info("Navigate to Terms and Conditions screen");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Terms and Conditions']")).click();
    }
    public static void PressPrivacyNotice(AppiumDriver<MobileElement> driver){
        log.info("Navigate to Privacy Notice screen");
        driver.findElement(By.xpath("//android.widget.TextView[@text='Privacy Notice']")).click();
    }
    public static void PressWhatCanIDoWithGCash(AppiumDriver<MobileElement> driver){
        log.info("Navigate to What can I do with GCash? screen");
        driver.findElement(By.xpath("//android.widget.TextView[@text='What can I do with GCash?']")).click();
    }
}
