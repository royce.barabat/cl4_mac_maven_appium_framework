package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class ShowMorePage {

    public static void pressManageGCredit(AppiumDriver<MobileElement> driver){
        MobileElement manage_credit_button = driver.findElement(By.xpath("//android.widget.TextView[@text='Manage Credit']"));
        manage_credit_button.click();
    }
    public static void pressBookMovies(AppiumDriver<MobileElement> driver){
        MobileElement cash_out_button = driver.findElement(By.xpath("//android.widget.TextView[@text='Cash-Out']"));
        cash_out_button.click();
    }



}
