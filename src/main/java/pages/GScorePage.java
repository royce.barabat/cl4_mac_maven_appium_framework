package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class GScorePage {
    /*
    This class contains the components and methods inside the GScore page.
    Author: Mariz Galibut
     */
    public static void pressPersonalInfoTab(AppiumDriver<MobileElement> driver){
        driver.findElement(By.xpath("//android.widget.TextView[@text='PERSONAL INFO']")).click();
    }
    public static void pressGScoreTab(AppiumDriver<MobileElement> driver){
        driver.findElement(By.xpath("//android.widget.TextView[@text='GSCORE']")).click();
    }
    public static void pressInfoButton(AppiumDriver<MobileElement> driver){
        driver.findElement(By.id("action_info")).click();
    }
    public static void pressManageCreditButton(AppiumDriver<MobileElement> driver){
        MobileElement manage_credit_button =driver.findElement(By.id("btn_click"));
        manage_credit_button.click();
    }
}