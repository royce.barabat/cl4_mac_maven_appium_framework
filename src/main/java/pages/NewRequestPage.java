package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class NewRequestPage {

    //Locate then press the Send Money elements
    public static void populateFieldRequestFrom(AppiumDriver<MobileElement> driver, String requestFrom) {
        driver.findElement(By.id("etRequestContact")).sendKeys(requestFrom);
    }
    public static void populateFieldAmount(AppiumDriver<MobileElement> driver, String amount) {
        driver.findElement(By.id("etRequestAmount")).sendKeys(amount);
    }
    public static void populateFieldNote(AppiumDriver<MobileElement> driver, String note) {
        driver.findElement(By.id("etRequestMessage")).sendKeys(note);
    }
    public static void clickButtonNext(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.id("viewRequestAction")).click();
    }

}
