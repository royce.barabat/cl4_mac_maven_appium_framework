package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class SendAngPaoPage {
	
	//Locate then populate/press the Send Ang Pao elements
	public static void populateFieldQuantity(AppiumDriver<MobileElement> driver, String quantity) {
		driver.findElement(By.xpath("//android.widget.EditText[@index=5]")).sendKeys(quantity);
	}
	public static void populateFieldAmount(AppiumDriver<MobileElement> driver, String amount) {
		driver.findElement(By.xpath("//android.widget.EditText[@index=8]")).sendKeys(amount);
	}
	public static void populateFieldMessage(AppiumDriver<MobileElement> driver, String message) {
		driver.findElement(By.xpath("//android.widget.EditText[@index=12]")).sendKeys(message);
	}
	public static void populateFieldNumberOne(AppiumDriver<MobileElement> driver, String numberOne) {
		driver.findElement(By.xpath("//android.widget.EditText[@index=6]")).sendKeys(numberOne);
	}
	public static void populateFieldNumberTwo(AppiumDriver<MobileElement> driver, String numberTwo) {
		driver.findElement(By.xpath("//android.widget.EditText[@index=9]")).sendKeys(numberTwo);
	}
	public static void populateButtonNext(AppiumDriver<MobileElement> driver) {
		driver.findElement(By.xpath("//android.widget.Button[@index=1]")).click();
	}
	
}
