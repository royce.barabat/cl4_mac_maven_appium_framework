package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class BuyLoadForPage {

    //Locate then press the tabs and the Next button
    public static void pressTabRegular(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='REGULAR']")).click();
    }
    public static void pressTabSurf(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='SURF']")).click();
    }
    public static void pressTabAllIn(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='ALL-IN']")).click();
    }
    public static void pressTabGamingPins(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='GAMING PINS']")).click();
    }
    public static void pressTabBroadband(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='BROADBAND']")).click();
    }
    public static void pressButtonNext(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='NEXT']")).click();
    }


    //Locate then press the Regular buttons
    public static void pressButton10(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='10']")).click();
    }
    public static void pressButton20(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='20']")).click();
    }
    public static void pressButton30(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='30']")).click();
    }
    public static void pressButton50(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='50']")).click();
    }
    public static void pressButton100(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='100']")).click();
    }
    public static void pressButton150(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='150']")).click();
    }
    public static void pressButton450(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='450']")).click();
    }
    public static void pressButton600(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='600']")).click();
    }
    public static void pressButton900(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='900']")).click();
    }


    //Locate then press the Surf buttons
    public static void pressButtonGoSurf50(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='GP GoSurf50']")).click();
    }
    public static void pressButtonGoSurf15(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='GP GoSurf15']")).click();
    }
    public static void pressButtonGoSurf299(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='GP GoSurf299']")).click();
    }
    public static void pressButtonGoSurf599(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='GP GoSurf599']")).click();
    }

    //Locate then press the All-in buttons
    public static void pressButtonGoUnli20(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='GP GoUnli20']")).click();
    }
    public static void pressButtonGoUnli25(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='GP GoUnli25']")).click();
    }
    public static void pressButtonGoUnli30(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='GP GoUnli30']")).click();
    }
    public static void pressButtonGoSakto70(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='GP GoSakto70']")).click();
    }
    public static void pressButtonGotsComboDD70(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='GotsComboDD70']")).click();
    }
    public static void pressButtonGoSakto90(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='GP GoSakto90']")).click();
    }

    //Locate then press the Gaming Pins buttons
    public static void pressButton50Steam(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='50 Steam Wallet Codes']")).click();
    }
    public static void pressButton100Steam(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='100 Steam Wallet Codes']")).click();
    }
    public static void pressButton250Steam(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='250 Steam Wallet Codes']")).click();
    }
    public static void pressButton500Steam(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='500 Steam Wallet Codes']")).click();
    }
    public static void pressButton800Steam(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='800 Steam Wallet Codes']")).click();
    }
    public static void pressButton1000Steam(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='1000 Steam Wallet Codes']")).click();
    }

    //Locate then press the Broadband buttons
    public static void pressButtonHomeSURF15(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Broadband HomeSURF15']")).click();
    }
    public static void pressButtonGoSURF50(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Broadband GoSURF50']")).click();
    }
    public static void pressButtonHomeSURF199(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Broadband HomeSURF199']")).click();
    }
    public static void pressButtonHomeSURF599(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Broadband HomeSURF599']")).click();
    }
    public static void pressButtonHomeSURF999(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Broadband HomeSURF999']")).click();
    }
    public static void pressButtonHomeSURF1499(AppiumDriver<MobileElement> driver) {
        driver.findElement(By.xpath("//android.widget.TextView[@text='Broadband HomeSURF1499']")).click();
    }


}
