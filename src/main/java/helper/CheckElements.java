package helper;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;


import java.util.logging.Logger;

public class CheckElements {

    private int retry = 10;
    private static final Logger log = Logger.getLogger("PayBillsPage");

    private String buildTextXpath(String name) {
        StringBuilder sb = new StringBuilder();
        sb.append("//android.widget.TextView[@text=");
        sb.append("\'");
        sb.append(name);
        sb.append("\'");
        sb.append("]");
        log.info("XPATH: " + sb.toString());
        return sb.toString();

    }

    public boolean isScreenReadyByXpath(AppiumDriver<MobileElement> driver, String xpathName) {
        if (xpathName.isEmpty()) {
            return false;
        } else {
            Boolean displayed = false;
            for (int i = 0; i < retry; i++) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                MobileElement screen = driver.findElement(By.xpath(this.buildTextXpath(xpathName)));
                displayed = screen.isDisplayed();

                log.info("Trying to determine screen.");
                if (displayed) {
                    log.info("Displayed");
                    break;
                }
                log.info("Counter " + i);
            }
            log.info("Returning screen");
            return displayed;
        }
    }

    public boolean isScreenReadyByID(AppiumDriver<MobileElement> driver, String id) {
        if (id.isEmpty()) {
            return false;
        } else {
            Boolean displayed = false;
            for (int i = 0; i < retry; i++) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                MobileElement screen = driver.findElement(By.id(id));
                displayed = screen.isDisplayed();

                log.info("Trying to determine screen.");
                if (displayed) {
                    log.info("Displayed");
                    break;
                }
                log.info("Counter " + i);
            }
            log.info("Returning screen");
            return displayed;
        }
    }
}