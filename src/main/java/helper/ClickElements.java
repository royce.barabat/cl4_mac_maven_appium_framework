package helper;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

import java.util.logging.Logger;

public class ClickElements {
    private int retry = 10;
    private static final Logger log = Logger.getLogger("PayBillsPage");

    private String buildXpath(String name) {
        StringBuilder sb = new StringBuilder();
        sb.append("//android.widget.TextView[@text=");
        sb.append("\'");
        sb.append(name);
        sb.append("\'");
        sb.append("]");
        log.info("XPATH: " + sb.toString());
        return sb.toString();

    }

    public boolean clickElementByXpath(AppiumDriver<MobileElement> driver, String xpathName) {
        if (xpathName.isEmpty()) {
            return false;
        } else {
            MobileElement locator = driver.findElement(By.xpath(this.buildXpath(xpathName)));
            if (!locator.isDisplayed()) {
                return false;
            } else {
                locator.click();
                return true;
            }
        }
    }

    public boolean clickElementById(AppiumDriver<MobileElement> driver, String id) {
        if (id.isEmpty()) {
            return false;
        } else {
            MobileElement locator = driver.findElement(By.id(id));
            if (!locator.isDisplayed()) {
                return false;
            } else {
                locator.click();
                return true;
            }
        }
    }

}

