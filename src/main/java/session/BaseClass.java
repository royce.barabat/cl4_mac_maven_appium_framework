package session;

//import com.aventstack.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentReports;
//import com.aventstack.extentreports.ExtentTest;
import com.relevantcodes.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.seleniumhq.jetty9.util.IO;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

//@Listeners(session.CaptureScreenShot.class)

public class BaseClass {

    static WebDriverWait driverWait;
    public static AppiumDriver driver;
    public static final Logger log = Logger.getLogger(". . . .Starting Appium driver");

//    //Click Method
//    public void click(By elementLocation) {
//        driver.findElement(elementLocation).click();
//    }
//
//    //Write Text
//    public void writeText(By elementLocation, String text) {
//        driver.findElement(elementLocation).sendKeys(text);
//    }
//
//    //Read Text
//    public String readText(By elementLocation) {
//        return driver.findElement(elementLocation).getText();
//    }
//
//    //Wait
//    public void waitVisibility(By by){
//        driverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
//    }
//

    protected  static WebElement waitForClickable(By locator){
        return driverWait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    protected static boolean elementIsNotPresent(By by){
        try{
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            return driver.findElements(by).isEmpty();
        }finally {
            driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        }
    }

    public void changeContext(String context) throws InterruptedException{
        Set<String> contextHandles = driver.getContextHandles();

        for(String s: contextHandles){
            System.out.println("Context : "+ s);
            if(s.contains(context)){
                System.out.println("Setting context to "+s);
                driver.context(s);
                break;
            }
        }
    }

    @BeforeSuite(alwaysRun = true)
    //@BeforeTest
    public static void setUp() throws IOException{

       // AppiumServer.startAppiumServer(); //to be implemented next

        //FileInputStream file =new FileInputStream(System.getProperty("user.dir")+"src/test/user.properties");
        //prop.loadfile(file);

        //killUiAutomatorServer();
//        DesiredCapabilities caps = new DesiredCapabilities();
//        caps.setCapability("deviceName", "My Phone");
//        //YGC6R19316011298 - SU 8.1.0
//        //988a90464d37544d4c30 - testing device 8.0.0
//        //92011ef8a8b215bd - j7 6.0.1
//        caps.setCapability("udid", "92011ef8a8b215bd"); //Give Device ID of your mobile phone
//        caps.setCapability("platformName", "Android");
//        caps.setCapability("platformVersion", "6.0.1");
//        caps.setCapability("appPackage", "com.globe.gcash.android");//Prod
//        //caps.setCapability("appPackage", "com.globe.gcash.android.uat.tokyo");//UAT
//        caps.setCapability("appActivity", "gcash.module.splashscreen.mvp.view.SplashScreenActivity");
//        caps.setCapability("noReset", "true");

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.DEVICE_NAME,"My Phone");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        caps.setCapability(MobileCapabilityType.UDID,"92011ef8a8b215bd");
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION,"6.0.1");
        caps.setCapability(AndroidMobileCapabilityType.NO_SIGN,"true");
        caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE,"com.globe.gcash.android.uat.tokyo");
        caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY,"gcash.module.splashscreen.mvp.view.SplashScreenActivity");
        //caps.setCapability("autoGrantPermission","true");//grant permission to system dialogues
        //caps.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir") + "/app/app-debug.apk");//for debug apk
        caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,20);
        //caps.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");//uiautomator
        caps.setCapability(MobileCapabilityType.NO_RESET,"true");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        driverWait = new WebDriverWait(driver,20);
        log.info(". . . . INITIALIZING EXECUTION");

    }

//    private void killUiAutomatorServer() throws IOException,InterruptedException{
//        Process process = Runtime.getRuntime().exec("adb uninstall io.appiumuiautomator2.server");
//        process.waitFor();
//
//        Process process2 = Runtime.getRuntime().exec("adb uninstall io.apiumautomator2.server.test");
//        process2.waitFor();
//    }

//    @AfterMethod
//    public static void setTestResult(ITestResult result) throws IOException {

//        String filename = FilenameUtils.removeExtension(result.getName());

//        String screenShot = CaptureScreens.captureScreen(CaptureScreens.generateFileName(result));
//          CaptureScreens.captureScreen(CaptureScreens.generateFileName(result));
//        if (result.getStatus() == ITestResult.FAILURE) {
//            //test.log(Status.FAIL, result.getName());
//            //test.log(Status.FAIL, result.getThrowable());
//            //test.fail("Screen Shot : " + test.addScreenCaptureFromPath(screenShot));
//        } else if (result.getStatus() == ITestResult.SUCCESS) {
//            test = reports.createTest(filename); //ok
//            //test.log(Status.PASS, result.getName());//ok
//            //test.log(Status.PASS, result.getThrowable());
//            test.pass("Screen Shot : " + test.addScreenCaptureFromPath(screenShot));//wrong png path
//            //test.log(LogStatus.INFO,"Success. . .");
//
//        } else if (result.getStatus() == ITestResult.SKIP) {
//            test.skip("Test Case : " + result.getName() + " has been skipped");
//        }


   // reports.flush();
//    }


    @AfterTest
    public void tearDown(){
        log.info(". . . . .Appium driver closed successfully");
        driver.quit();
        //AppiumServer.stopAppiumServer();
    }

}
