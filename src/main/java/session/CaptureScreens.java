package session;

import io.appium.java_client.AppiumDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaptureScreens extends BaseClass {

    private static final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy__hh:mm:ss:aa");


    public static String captureScreen(String screenName) throws IOException{

        TakesScreenshot screen = driver;
        File src = screen.getScreenshotAs(OutputType.FILE);
        String dest = System.getProperty("user.dir")+"/Screenshot/"+screenName+".png";
        File target = new File(dest);
        FileUtils.copyFile(src, target);

        return dest;
    }

    public static String generateFileName(ITestResult result){
        Date date = new Date();
        String fileName = result.getName()+ "_" + dateFormat.format(date);

        return fileName;
    }

    public static boolean deleteDir(File dir) {
        if(dir.isDirectory()){
            String[] children = dir.list();
            assert children != null;
            for (String aChildren : children){
                boolean success = deleteDir(new File(dir, aChildren));
                if (!success){
                    return false;
                }
            }
        }
        return dir.delete();
    }

}
