package session;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static session.BaseClass.driver;

public class CaptureScreenShot implements ITestListener {

    private static boolean deleteDir(File dir) {
        if(dir.isDirectory()){
            String[] children = dir.list();
            assert children != null;
            for (String aChildren : children){
                boolean success = deleteDir(new File(dir, aChildren));
                if (!success){
                    return false;
                }
            }
        }
        return dir.delete();
    }

//    public static void  captureScreenShots() throws IOException {
//
//        String folder_name;
//        DateFormat df;
//
//        folder_name = "screenshot";
//        File f = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//        //Date format fot screenshot file name
//        df = new SimpleDateFormat("dd-MMM-yyyy_hh:mm:ss_aa");
//        //create dir with given folder name
//        new File(folder_name).mkdir();
//        //Setting file name
//        String file_name=df.format(new Date())+".png";
//        //copy screenshot file into screenshot folder.
//        FileUtils.copyFile(f, new File(folder_name + "/" + file_name));
//    }

    @Override
    public void onStart(ITestContext iTestContext) {
        deleteDir(new File("Screenshot"));
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        deleteDir(new File("Screenshot"));
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        captureScreenShot(iTestResult, "pass");
        System.out.println("Testcase pass : "+iTestResult.getInstanceName());
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        captureScreenShot(iTestResult, "fail");
        System.out.println("Testcase fail : "+iTestResult.getInstanceName());
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }

    private void captureScreenShot(ITestResult result, String status){

        String destDir = "";
        String passFailMethod = result.getMethod().getRealClass().getSimpleName() + "." + result.getMethod().getMethodName();
        //to capture screenshot
        File srcFile = driver.getScreenshotAs(OutputType.FILE);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy__hh:mm:ss:aa");
        //if status = fail then set folder name "screenshot/failures"
        if(status.equalsIgnoreCase("fail")){
            destDir = "Screenshot/Failures";
        }else if(status.equalsIgnoreCase("pass")){
            destDir = "Screenshot/Success";
        }
        //to create folder to store screenshots
        new File(destDir).mkdirs();
        //set file name with combination of test class name + date time
        String destFile = passFailMethod + " - " + dateFormat.format(new Date()) + ".png";

        try{
            //store file at destination folder location
            FileUtils.copyFile(srcFile,new File(destDir + "/" + destFile));
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}

